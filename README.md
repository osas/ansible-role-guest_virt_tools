# Ansible role for Virtualized Guests tooling

## Introduction

This role installs virtualization tools on guests.

Currently this role only handles KVM virtualization.

It installs qemu-guest-agent, which is a daemon program running inside
the domain which is supposed to help management applications with
executing functions which need assistance of the guest OS. For example,
freezing and thawing filesystems, entering suspend…

## Variables

none.

## TODO

Support more virtualization methods.
